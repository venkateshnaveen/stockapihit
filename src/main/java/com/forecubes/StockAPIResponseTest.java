package com.forecubes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.*;
import org.json.JSONObject;


public class StockAPIResponseTest {

	final static  Logger logger=Logger.getLogger(StockAPIResponseTest.class);

	public static void main(String[] args) {


		logger.info("this is info:" + Thread.currentThread().getName());
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpRequest = new HttpGet("https://api.iextrading.com/1.0/stock/market/batch?symbols="+"gOOG"+"&types=quote");
		HttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(httpRequest);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Get the response
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String line = "";
		StringBuffer responseData = new StringBuffer();
		try {
			while ((line = bufferedReader.readLine()) != null) {
				responseData.append(line);
			}
			JSONObject jsonObj1 = new JSONObject(responseData.toString());
			JSONObject jsonObj2 = jsonObj1.getJSONObject("gOOG".toUpperCase());
			JSONObject jsonObj3 = jsonObj2.getJSONObject("quote");
			logger.info("this is info: " + "ThreadName is : "+Thread.currentThread().getName()+" CompanyName is : "+jsonObj3.get("companyName"));


		} catch (IOException e) {
			logger.error(e);
		}}


}
